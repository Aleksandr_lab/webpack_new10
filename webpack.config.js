const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.js'
    },
    watch:true,
    watchOptions:{
        ignored: /node_modules/
    },
    devServer:{
        contentBase: path.resolve(__dirname, 'dist'),
        port: 8080
    },
    module: {
        rules:[
            {
                test: /\.html$/,
                use: [ {
                  loader: 'html-loader',
                  options: {
                    //minimize: true,
                    ///removeComments: false,
                    //collapseWhitespace: false
                  }
                }]
            },
            {
                test: /\.jpg|png|webp|gif|svg$/,
                use: {
                  loader: 'file-loader',
                  options: {
                    publicPath: 'src/assets/img',
                  },
                },
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.js$/i,
                exclude: /\.(node_modules)/,
                use:{
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env']
                        ]
                    }
                }
            }
        ]
        
    },
    plugins: [
        new HtmlWebpackPlugin({
          filename: 'index.html',
          title: 'My App',
          template: 'src/products.html'
        })
      ]
}